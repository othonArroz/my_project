#include <stdio.h>
#include "dice.h"

int main() {//inicio funcao main

    initializeSeed();
    int n;
    //printando o resultado do dado

    printf("Qual o numero de faces do dado?");
    scanf("%d", &n);

    printf("Let's roll the dice: %d\n", rollDice(n));
    return 0;
}
